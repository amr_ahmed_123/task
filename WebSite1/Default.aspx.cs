﻿using System;
using System.Web.UI.WebControls;
using System.IO;
using System.IO.Compression;
using System.Security.Cryptography;
using System.Linq;
using ServiceReference1;
using System.Security.Cryptography.X509Certificates;

public partial class _Default : System.Web.UI.Page
{

    protected void Button1_Click(object sender, EventArgs e)
    {
        FileUpload fu = FileUpload1;

        if (!fu.HasFile)
            return;

        Service1Client service = new Service1Client();
        using (Stream content = new MemoryStream())
        {
            X509Certificate2 publicCert = new X509Certificate2(@"C:\Users\amr\Desktop\task.cer");

            X509Certificate2 privateCert = null;
            X509Store store = new X509Store(StoreLocation.LocalMachine);
            store.Open(OpenFlags.ReadOnly);
            foreach (X509Certificate2 cert in store.Certificates)
            {
                if (cert.GetCertHashString() == publicCert.GetCertHashString())
                    privateCert = cert;
            }

            fu.FileContent.CopyTo(content);
            content.Position = 0;
            byte[] hash, signature;
            using (var rsa = new RSACryptoServiceProvider())
            {
                rsa.FromXmlString(privateCert.PrivateKey.ToXmlString(true));
                using (var sha = new SHA256CryptoServiceProvider())
                {
                    hash = sha.ComputeHash(content);
                    signature = rsa.SignHash(hash, HashAlgorithmName.SHA256, RSASignaturePadding.Pkcs1);
                }

                using (Aes aes = Aes.Create())
                {
                    aes.KeySize = 256;
                    aes.GenerateKey();
                    aes.GenerateIV();
                    aes.Padding = PaddingMode.PKCS7;
                    byte[] key = aes.Key, iv = aes.IV;
                    byte[] keys = rsa.Encrypt(key.Concat(iv).ToArray(), true);
                    using (var encrypted = new MemoryStream())
                    {
                        using (var crypto = new CryptoStream(encrypted, aes.CreateEncryptor(), CryptoStreamMode.Write))
                        {
                            using (var gzip = new GZipStream(crypto, CompressionMode.Compress, true))
                            {
                                content.Position = 0;
                                content.CopyTo(gzip);
                            }
                            crypto.FlushFinalBlock();
                            encrypted.Position = 0;
                            byte[] value = encrypted.ToArray();
                            var message = service.upload(value, signature, hash, keys);
                            Label1.Text = message;
                        }
                    }
                }
            }
        }
    }

}