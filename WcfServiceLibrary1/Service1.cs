﻿using System;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;

namespace WcfServiceLibrary1
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.

    public class Service1 : IService1
    {
        private static string publickey;
        private static string privatekey;
        static Service1()
        {
            X509Certificate2 publicCert = new X509Certificate2(@"C:\Users\amr\Desktop\task.cer");

            X509Certificate2 privateCert = null;
            X509Store store = new X509Store(StoreLocation.LocalMachine);
            store.Open(OpenFlags.ReadOnly);
            foreach (X509Certificate2 cert in store.Certificates)
            {
                if (cert.GetCertHashString() == publicCert.GetCertHashString())
                    privateCert = cert;
            }
            RSACryptoServiceProvider key = new RSACryptoServiceProvider();
            key.FromXmlString(privateCert.PrivateKey.ToXmlString(true));
            privatekey = key.ToXmlString(true);
            publickey = key.ToXmlString(false);
        }
        public string upload(byte[] value, byte[] sign, byte[] Hash, byte[] encryptionkeys)
        {
            byte[] keys;
            using (var rsa = new RSACryptoServiceProvider())
            {
                rsa.FromXmlString(privatekey);
                keys = rsa.Decrypt(encryptionkeys, true);
                if (!rsa.VerifyHash(Hash, sign, HashAlgorithmName.SHA256, RSASignaturePadding.Pkcs1))
                    return "Invalid Signature";
            }
            using (var aes = Aes.Create())
            {
                aes.Key = keys.Take(32).ToArray();
                aes.IV = keys.Skip(32).ToArray();
                aes.Padding = PaddingMode.PKCS7;
                using (var decrypted = new MemoryStream())
                {
                    using (var encrypted = new MemoryStream(value))
                    using (var crypto = new CryptoStream(encrypted, aes.CreateDecryptor(), CryptoStreamMode.Read))
                    using (var gzip = new GZipStream(crypto, CompressionMode.Decompress))
                    {
                        gzip.CopyTo(decrypted);
                    }
                    
                    decrypted.Position = 0;
                    using (var sha = new SHA256CryptoServiceProvider())
                    {
                        if (!Hash.SequenceEqual(sha.ComputeHash(decrypted)))
                            return "Hash Mismatch";
                    }
                    decrypted.Position = 0;
                    using (var fs = File.Open($@"C:\Users\amr\Desktop\output{DateTime.Now.Ticks}.txt", FileMode.OpenOrCreate))
                        decrypted.CopyTo(fs);
                }
            }

            return "file uploaded Successfully";
        }

    }
}
